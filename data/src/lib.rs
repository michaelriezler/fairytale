#[macro_use]
extern crate diesel;
extern crate actix;
extern crate chrono;
extern crate serde_json;
extern crate uuid;

pub mod models;
pub mod schema;

use actix::prelude::*;
use chrono::naive::NaiveDate;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::result::Error;
use models::{Author, AuthorBook, Book};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

pub struct DBExecutor {
  pub connection: PgConnection,
}

impl DBExecutor {
  pub fn new(url: &str) -> DBExecutor {
    let connection =
      PgConnection::establish(&url).unwrap_or_else(|_| panic!("Error connecting to {}", url));

    DBExecutor { connection }
  }
}

impl Actor for DBExecutor {
  type Context = SyncContext<Self>;
}

#[derive(Deserialize, Serialize, Clone)]
pub struct BookCreate {
  pub body: serde_json::Value,
  pub slug: String,
  pub title: String,
  pub cover: Option<String>,
  pub release_date: NaiveDate,
  pub summary: String,
  pub lang: String,
  pub keywords: String,
  pub author_id: uuid::Uuid,
}

impl Message for BookCreate {
  type Result = Result<uuid::Uuid, Error>;
}

impl Handler<BookCreate> for DBExecutor {
  type Result = Result<uuid::Uuid, Error>;

  fn handle(&mut self, msg: BookCreate, _: &mut Self::Context) -> Self::Result {
    use self::schema::authorbook;
    use self::schema::book as b;
    use self::schema::book::dsl::book;

    let book_id = Uuid::new_v4();

    let new_book = (
      b::id.eq(book_id),
      b::slug.eq(msg.slug),
      b::body.eq(msg.body),
      b::title.eq(msg.title),
      b::cover.eq(msg.cover),
      b::release_date.eq(msg.release_date),
      b::summary.eq(msg.summary),
      b::lang.eq(msg.lang),
      b::keywords.eq(msg.keywords),
    );

    diesel::insert_into(book)
      .values(new_book)
      .execute(&self.connection)?;

    diesel::insert_into(authorbook::table)
      .values((
        authorbook::author_id.eq(msg.author_id),
        authorbook::book_id.eq(book_id),
      ))
      .execute(&self.connection)?;

    Ok(book_id)
  }
}

#[derive(Deserialize, Serialize, Clone)]
pub struct BookUpdate {
  pub id: uuid::Uuid,
  pub body: serde_json::Value,
  pub slug: String,
  pub title: String,
  pub cover: Option<String>,
  pub release_date: NaiveDate,
  pub summary: String,
  pub lang: String,
  pub keywords: String,
  pub author_id: uuid::Uuid,
}

impl Message for BookUpdate {
  type Result = Result<(), Error>;
}

impl Handler<BookUpdate> for DBExecutor {
  type Result = Result<(), Error>;

  fn handle(&mut self, msg: BookUpdate, _: &mut Self::Context) -> Self::Result {
    use self::schema::authorbook as ab;
    use self::schema::book::dsl::*;

    diesel::update(book.filter(id.eq(msg.id)))
      .set((
        body.eq(msg.body),
        slug.eq(msg.slug),
        title.eq(msg.title),
        cover.eq(msg.cover),
        release_date.eq(msg.release_date),
        summary.eq(msg.summary),
        lang.eq(msg.lang),
        keywords.eq(msg.keywords),
      ))
      .execute(&self.connection)?;

    diesel::update(ab::table.filter(ab::book_id.eq(msg.id)))
      .set(ab::author_id.eq(msg.author_id))
      .execute(&self.connection)?;

    Ok(())
  }
}

pub struct BookGet {
  pub id: uuid::Uuid,
}

type BookResult = Option<(Book, AuthorBook)>;

impl Message for BookGet {
  type Result = Result<BookResult, Error>;
}

impl Handler<BookGet> for DBExecutor {
  type Result = Result<BookResult, Error>;

  fn handle(&mut self, msg: BookGet, _: &mut Self::Context) -> Self::Result {
    use self::schema::authorbook::dsl::*;
    use self::schema::book::dsl::{book, id};

    let book_author = authorbook.on(book_id.eq(id));

    let mut books: Vec<(Book, AuthorBook)> = book
      .filter(id.eq(msg.id))
      .inner_join(book_author)
      .limit(1)
      .load(&self.connection)
      .expect("Book get error");

    Ok(books.pop())
  }
}

pub struct BookList;

#[derive(Queryable, Serialize)]
pub struct BookListItem {
  pub id: uuid::Uuid,
  pub title: String,
  pub cover: Option<String>,
  pub release_date: NaiveDate,
  pub lang: String,
}

type BookListResult = Vec<BookListItem>;

impl Message for BookList {
  type Result = Result<BookListResult, Error>;
}

impl Handler<BookList> for DBExecutor {
  type Result = Result<BookListResult, Error>;

  fn handle(&mut self, _msg: BookList, _: &mut Self::Context) -> Self::Result {
    use self::schema::book::dsl::*;

    let books: BookListResult = book
      .select((id, title, cover, release_date, lang))
      .load(&self.connection)?;

    Ok(books)
  }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct AuthorCreate {
  pub name: String,
  pub username: String,
  pub bio: String,
}

impl Message for AuthorCreate {
  type Result = Result<uuid::Uuid, Error>;
}

impl Handler<AuthorCreate> for DBExecutor {
  type Result = Result<uuid::Uuid, Error>;

  fn handle(&mut self, msg: AuthorCreate, _: &mut Self::Context) -> Self::Result {
    use self::schema::author as a;
    use self::schema::author::dsl::author;

    let id = Uuid::new_v4();

    let new_author = (
      a::id.eq(id),
      a::name.eq(msg.name),
      a::username.eq(msg.username),
      a::bio.eq(msg.bio),
    );

    diesel::insert_into(author)
      .values(new_author)
      .execute(&self.connection)?;

    Ok(id)
  }
}

pub struct AuthorGet {
  pub id: uuid::Uuid,
}

impl Message for AuthorGet {
  type Result = Result<Option<Author>, Error>;
}

impl Handler<AuthorGet> for DBExecutor {
  type Result = Result<Option<Author>, Error>;

  fn handle(&mut self, msg: AuthorGet, _: &mut Self::Context) -> Self::Result {
    use self::schema::author::dsl::{author, id};

    let mut authors: Vec<Author> = author.filter(id.eq(msg.id)).load(&self.connection)?;

    Ok(authors.pop())
  }
}

pub struct AuthorList;

impl Message for AuthorList {
  type Result = Result<Vec<Author>, Error>;
}

impl Handler<AuthorList> for DBExecutor {
  type Result = Result<Vec<Author>, Error>;

  fn handle(&mut self, _msg: AuthorList, _: &mut Self::Context) -> Self::Result {
    use self::schema::author;
    let authors: Vec<Author> = author::table.load(&self.connection)?;
    Ok(authors)
  }
}

#[derive(Deserialize, Serialize, Clone)]
pub struct AuthorUpdate {
  pub id: uuid::Uuid,
  pub name: String,
  pub username: String,
  pub bio: String,
}

impl Message for AuthorUpdate {
  type Result = Result<(), Error>;
}

impl Handler<AuthorUpdate> for DBExecutor {
  type Result = Result<(), Error>;

  fn handle(&mut self, msg: AuthorUpdate, _: &mut Self::Context) -> Self::Result {
    use self::schema::author::dsl::*;

    diesel::update(author.filter(id.eq(msg.id)))
      .set((
        name.eq(msg.name),
        username.eq(msg.username),
        bio.eq(msg.bio),
      ))
      .execute(&self.connection)?;

    Ok(())
  }
}
