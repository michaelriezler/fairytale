use crate::schema::author;
use crate::schema::authorbook;
use crate::schema::book;

use chrono::naive::{NaiveDate, NaiveDateTime};
use serde::{Deserialize, Serialize};
use uuid;

#[derive(Deserialize, Serialize, Clone, Queryable)]
pub struct Chapter {
  title: String,
  value: String,
}

pub type BookBody = Vec<Chapter>;

#[table_name = "book"]
#[derive(Queryable, Insertable, Serialize)]
pub struct Book {
  pub id: uuid::Uuid,
  pub slug: String,
  pub body: serde_json::Value,
  pub title: String,
  pub cover: Option<String>,
  pub release_date: NaiveDate,
  pub summary: String,
  pub lang: String,
  pub keywords: String,
  pub created_at: NaiveDateTime,
  pub updated_at: NaiveDateTime,
}

#[table_name = "author"]
#[derive(Queryable, Insertable, Serialize)]
pub struct Author {
  pub id: uuid::Uuid,
  pub name: String,
  pub username: String,
  pub bio: String,
  pub created_at: NaiveDateTime,
  pub updated_at: NaiveDateTime,
}

#[table_name = "authorbook"]
#[derive(Queryable, Insertable, Serialize)]
pub struct AuthorBook {
  pub author_id: uuid::Uuid,
  pub book_id: uuid::Uuid,
  pub created_at: NaiveDateTime,
  pub updated_at: NaiveDateTime,
}
