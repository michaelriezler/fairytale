table! {
    adminuser (id) {
        id -> Uuid,
        email -> Varchar,
        password -> Text,
    }
}

table! {
    author (id) {
        id -> Uuid,
        name -> Varchar,
        username -> Varchar,
        bio -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    authorbook (author_id, book_id) {
        author_id -> Uuid,
        book_id -> Uuid,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    book (id) {
        id -> Uuid,
        slug -> Varchar,
        body -> Json,
        title -> Varchar,
        cover -> Nullable<Varchar>,
        release_date -> Date,
        summary -> Varchar,
        lang -> Varchar,
        keywords -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    bookmeta (id) {
        id -> Uuid,
        book_id -> Nullable<Uuid>,
        views -> Int4,
        add_to_homescreen -> Int4,
        downloads -> Int4,
        time_on_book -> Int4,
    }
}

joinable!(authorbook -> author (author_id));
joinable!(authorbook -> book (book_id));
joinable!(bookmeta -> book (book_id));

allow_tables_to_appear_in_same_query!(
    adminuser,
    author,
    authorbook,
    book,
    bookmeta,
);
