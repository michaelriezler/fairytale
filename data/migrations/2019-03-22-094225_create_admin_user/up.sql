-- Your SQL goes here

create table AdminUser (
  id uuid primary key default gen_random_uuid(),
  email varchar(254) not null,
  password text not null
)
