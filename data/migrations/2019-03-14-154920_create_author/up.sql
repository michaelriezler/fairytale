
create table Author (
  id uuid primary key default gen_random_uuid(),
  name varchar(255) not null,
  username varchar(80) not null unique,
  bio varchar(1000) not null default '',
  created_at timestamptz not null default now(),
  updated_at timestamptz not null default now()
);

create trigger set_timestamp_author
before update on Author
for each row
execute procedure trigger_set_timestamp();

create index author_username_index on Author(username);