
drop table if exists Author;
drop trigger if exists set_timestamp_author on Book;
drop index if exists author_username_index;