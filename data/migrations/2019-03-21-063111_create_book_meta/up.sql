-- Your SQL goes here

create table BookMeta (
  id uuid primary key default gen_random_uuid(),
  book_id uuid references Book(id),
  views int not null default 0,
  add_to_homescreen int not null default 0,
  downloads int not null default 0,
  time_on_book int not null default 0
);

create index fk_book_id_meta on BookMeta(book_id);