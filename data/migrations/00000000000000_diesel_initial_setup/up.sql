-- This file was automatically created by Diesel to setup helper functions
-- and other internal bookkeeping. This file is safe to edit, any future
-- changes will be added to existing projects as new migrations.




-- Sets up a trigger for the given table to automatically set a column called
-- `updated_at` whenever the row is modified (unless `updated_at` was included
-- in the modified columns)
--
-- # Example
--
-- ```sql
-- CREATE TABLE users (id SERIAL PRIMARY KEY, updated_at TIMESTAMP NOT NULL DEFAULT NOW());
--
-- SELECT diesel_manage_updated_at('users');
-- ```
create or replace function diesel_manage_updated_at(_tbl regclass) returns void as $$
begin
    execute format('create trigger set_updated_at before update on %s
                    for each row execute procedure diesel_set_updated_at()', _tbl);
end;
$$ language plpgsql;

create or replace function diesel_set_updated_at() returns trigger as $$
begin
    if (
        new is distinct from old and
        new.updated_at is not distinct from old.updated_at
    ) then
        new.updated_at := current_timestamp;
    end if;
    return new;
end;
$$ language plpgsql;

create or replace function trigger_set_timestamp()
returns trigger as $$
begin
  new.updated_at = now();
  return new;
end;
$$ language plpgsql;


create extension if not exists pgcrypto;


-- postgrest api user

do
$do$
begin
   if not exists (
      select                       -- select list can stay empty for this
      from   pg_catalog.pg_roles
      where  rolname = 'web_anon') then

      create role web_anon nologin;
      grant usage on schema public to web_anon;
      grant select on all tables in schema public to web_anon;
   end if;
end
$do$;

do
$do$
begin
   if not exists (
      select                       -- select list can stay empty for this
      from   pg_catalog.pg_roles
      where  rolname = 'authenticator') then

      create role authenticator noinherit login password 'jW{CPOP5;)OCYO.E.!d7vcG_pPtX';
      grant web_anon to authenticator;
   end if;
end
$do$;



-- http://postgrest.org/en/v5.2/admin.html#schema-reloading
create or replace function notify_ddl_postgrest()
  returns event_trigger
 language plpgsql
  as $$
begin
  notify ddl_command_end;
end;
$$;

create event trigger ddl_postgrest on ddl_command_end
   execute procedure notify_ddl_postgrest();