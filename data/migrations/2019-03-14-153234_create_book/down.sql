
drop table if exists Book;
drop trigger if exists set_timestamp_book on Book;
drop index if exists book_slug_index;
