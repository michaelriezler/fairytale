
create table Book (
  id uuid primary key default gen_random_uuid(),
  slug varchar(2000) not null,
  body json not null,
  title varchar(255) not null,
  cover varchar(2000),
  release_date date not null,
  summary varchar(1500) not null default '',
  lang varchar(7) not null default 'en',
  keywords text not null default '',
  created_at timestamptz not null default now(),
  updated_at timestamptz not null default now()
);

create trigger set_timestamp_book
before update on Book
for each row
execute procedure trigger_set_timestamp();

create index book_slug_index on Book(slug);