
create table AuthorBook (
  author_id uuid references Author on delete cascade,
  book_id uuid references Book on delete cascade,
  created_at timestamptz not null default now(),
  updated_at timestamptz not null default now(),
  primary key (author_id, book_id)
);

create trigger set_timestamp_author_book
before update on AuthorBook
for each row
execute procedure trigger_set_timestamp();