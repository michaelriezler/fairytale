use crate::bad_format;
use crate::missing_query;
use crate::APIResponse;
use crate::AppState;

use actix_web::AsyncResponder;
use actix_web::Json;
use actix_web::State;
use actix_web::{HttpRequest, HttpResponse};
use data::*;
use futures::Future;
use uuid::Uuid;

pub fn get(req: &HttpRequest<AppState>) -> APIResponse {
  let query = req.query();
  let id = match query.get("id") {
    Some(id) => id,
    None => return missing_query("id".to_string()),
  };

  let id = match Uuid::parse_str(id) {
    Ok(id) => id,
    Err(_) => return bad_format("Expected id to be a valid uuid/v4".to_string()),
  };

  req
    .state()
    .db
    .send(AuthorGet { id })
    .from_err()
    .and_then(|res| match res {
      Ok(author) => Ok(HttpResponse::Ok().json(author)),
      Err(_) => Ok(HttpResponse::InternalServerError().into()),
    })
    .responder()
}

pub fn create(state: State<AppState>, msg: Json<AuthorCreate>) -> APIResponse {
  let author = AuthorCreate {
    name: msg.name.clone(),
    username: msg.username.clone(),
    bio: msg.bio.clone(),
  };

  state
    .db
    .send(author)
    .from_err()
    .and_then(|res| match res {
      Ok(id) => Ok(HttpResponse::Ok().json(id)),
      Err(_) => Ok(HttpResponse::InternalServerError().into()),
    })
    .responder()
}

pub fn update(state: State<AppState>, msg: Json<AuthorUpdate>) -> APIResponse {
  let author = AuthorUpdate {
    id: msg.id.clone(),
    name: msg.name.clone(),
    username: msg.username.clone(),
    bio: msg.bio.clone(),
  };

  state
    .db
    .send(author)
    .from_err()
    .and_then(|res| match res {
      Ok(_id) => Ok(HttpResponse::Ok().into()),
      Err(_) => Ok(HttpResponse::InternalServerError().into()),
    })
    .responder()
}

pub fn list(req: &HttpRequest<AppState>) -> APIResponse {
  req
    .state()
    .db
    .send(AuthorList)
    .from_err()
    .and_then(|res| match res {
      Ok(books) => Ok(HttpResponse::Ok().json(books)),
      Err(_) => Ok(HttpResponse::InternalServerError().into()),
    })
    .responder()
}
