extern crate actix_web;
extern crate diesel;
extern crate dotenv;
extern crate failure;
extern crate futures;
extern crate serde_json;
extern crate uuid;

mod auth;
mod author;
mod book;

use crate::auth::AdminAuth;
use actix::prelude::*;
use actix_web::middleware::cors::Cors;
use actix_web::Error;
use actix_web::{
  http::{header, Method},
  server::HttpServer,
  App, HttpResponse,
};
use data::*;
use dotenv::dotenv;
use futures::future;
use futures::Future;
use std::env;

pub struct AppState {
  pub db: Addr<DBExecutor>,
}

type APIResponse = Box<Future<Item = HttpResponse, Error = Error>>;

fn main() {
  dotenv().ok();

  let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");

  let sys = actix::System::new("diesel-example");

  let db_pool_size = env::var("DB_POOL_SIZE").unwrap_or("3".to_string());
  let db_pool_size = db_pool_size
    .parse::<usize>()
    .expect("Expect DB_POOL_SIZE to be a number");

  let addr = SyncArbiter::start(db_pool_size, move || DBExecutor::new(&db_url));

  let origin = "http://localhost:3000";

  let app = HttpServer::new(move || {
    vec![
      App::with_state(AppState { db: addr.clone() })
        .prefix("/__/admin/book")
        .configure(|app| {
          Cors::for_app(app)
            .allowed_origin(origin)
            .allowed_methods(vec!["GET", "POST"])
            .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
            .allowed_header(header::CONTENT_TYPE)
            .max_age(3600)
            .resource("/get", |r| r.method(Method::GET).a(book::get_book))
            .resource("/create", |r| {
              r.middleware(AdminAuth);
              r.method(Method::POST).with_async(book::create_book)
            })
            .resource("/update", |r| {
              r.middleware(AdminAuth);
              r.method(Method::POST).with_async(book::update_book)
            })
            .resource("/list", |r| r.method(Method::GET).a(book::list_book))
            .register()
        }),
      App::with_state(AppState { db: addr.clone() })
        .prefix("/__/admin/author")
        .configure(|app| {
          Cors::for_app(app)
            .allowed_origin(origin)
            .allowed_methods(vec!["GET", "POST"])
            .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
            .allowed_header(header::CONTENT_TYPE)
            .max_age(3600)
            .resource("/get", |r| r.method(Method::GET).a(author::get))
            .resource("/create", |r| {
              r.middleware(AdminAuth);
              r.method(Method::POST).with_async(author::create)
            })
            .resource("/list", |r| r.method(Method::GET).a(author::list))
            .resource("/update", |r| {
              r.middleware(AdminAuth);
              r.method(Method::POST).with_async(author::update)
            })
            .register()
        }),
      App::with_state(AppState { db: addr.clone() })
        .prefix("/__/admin/admin")
        .configure(|app| {
          Cors::for_app(app)
            .allowed_origin(origin)
            .allowed_methods(vec!["GET", "POST"])
            .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
            .allowed_header(header::CONTENT_TYPE)
            .max_age(3600)
            .resource("/auth/test", |r| {
              r.middleware(AdminAuth);
              r.f(|_| HttpResponse::Ok())
            })
            .register()
        }),
    ]
  });

  let port = env::var("PORT").unwrap_or("8080".to_string());
  app.bind(format!("127.0.0.1:{}", port)).unwrap().start();

  println!("Started http server: 127.0.0.1:8080");

  let _ = sys.run();
}

fn missing_query(arg: String) -> APIResponse {
  let msg = format!("Query parameter {} missing", arg);
  Box::new(future::result(Ok(HttpResponse::BadRequest().body(msg))))
}

fn bad_format(msg: String) -> APIResponse {
  Box::new(future::result(Ok(HttpResponse::BadRequest().body(msg))))
}
