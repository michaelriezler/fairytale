use crate::AppState;
use actix_web::middleware::{Middleware, Started};
use actix_web::HttpRequest;
use actix_web::HttpResponse;
use actix_web::ResponseError;
use failure::Fail;
use std::env;

pub struct AdminAuth;

impl Middleware<AppState> for AdminAuth {
  fn start(&self, req: &HttpRequest<AppState>) -> actix_web::Result<Started> {
    if req.method() == "OPTIONS" {
      return Ok(Started::Done);
    }

    let token = req
      .headers()
      .get("AUTHORIZATION")
      .map(|value| value.to_str().ok())
      .ok_or(ServiceError::Unauthorized)?;

    match token {
      Some(token) => {
        if !verify_token(token.to_string()) {
          return Err(ServiceError::Unauthorized.into());
        }
        Ok(Started::Done)
      }

      None => Err(ServiceError::Unauthorized.into()),
    }
  }
}

fn verify_token(token: String) -> bool {
  if !token.starts_with("Basic") {
    return false;
  }

  let values: Vec<&str> = token["Basic: ".len()..].split(':').collect();

  let username = match values.get(0) {
    Some(value) => value,
    None => return false,
  };

  let password = match values.get(1) {
    Some(value) => value,
    None => return false,
  };

  let admin_username = env::var("ADMIN_USERNAME").expect("ADMIN_USERNAME not set");
  let admin_password = env::var("ADMIN_PASSWORD").expect("ADMIN_PASSWORD not set");

  if *username == admin_username && *password == admin_password {
    return true;
  }

  false
}

#[derive(Debug, Fail)]
pub enum ServiceError {
  #[fail(display = "Unauthorised")]
  Unauthorized,
}

impl ResponseError for ServiceError {
  fn error_response(&self) -> HttpResponse {
    match *self {
      ServiceError::Unauthorized => HttpResponse::Unauthorized().json("Unauthorised"),
    }
  }
}
