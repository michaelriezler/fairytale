use crate::bad_format;
use crate::missing_query;
use crate::APIResponse;
use crate::AppState;

use actix_web::AsyncResponder;
use actix_web::Json;
use actix_web::State;
use actix_web::{HttpRequest, HttpResponse};
use data::*;
use futures::future;
use futures::Future;
use uuid::Uuid;

pub fn get_book(req: &HttpRequest<AppState>) -> APIResponse {
  let query = req.query();
  let book_id = match query.get("id") {
    Some(id) => id,
    None => return missing_query("id".to_string()),
  };

  let book_id = match Uuid::parse_str(book_id) {
    Ok(id) => id,
    Err(_) => return bad_format("Expected id to be a valid uuid/v4".to_string()),
  };

  req
    .state()
    .db
    .send(BookGet { id: book_id })
    .from_err()
    .and_then(|res| match res {
      Ok(book) => Ok(HttpResponse::Ok().json(book)),
      Err(_) => Ok(HttpResponse::InternalServerError().into()),
    })
    .responder()
}

pub fn create_book(state: State<AppState>, book: Json<BookCreate>) -> APIResponse {
  let body = match serde_json::to_string(&book.body) {
    Ok(json) => json,
    Err(_) => {
      return Box::new(future::result(Ok(
        HttpResponse::InternalServerError().into(),
      )));
    }
  };

  let content: data::models::BookBody = match serde_json::from_str(&body) {
    Ok(content) => content,
    Err(_) => return bad_format("".to_string()),
  };

  let book = BookCreate {
    body: serde_json::to_value(content).unwrap(),
    title: book.title.clone(),
    cover: book.cover.clone(),
    release_date: book.release_date.clone(),
    lang: book.lang.clone(),
    author_id: book.author_id.clone(),
    keywords: book.keywords.clone(),
    slug: book.slug.clone(),
    summary: book.summary.clone(),
  };

  state
    .db
    .send(book)
    .from_err()
    .and_then(|res| match res {
      Ok(id) => Ok(HttpResponse::Ok().json(id)),
      Err(_) => Ok(HttpResponse::InternalServerError().into()),
    })
    .responder()
}

pub fn update_book(state: State<AppState>, book: Json<BookUpdate>) -> APIResponse {
  let body = match serde_json::to_string(&book.body) {
    Ok(json) => json,
    Err(_) => {
      return Box::new(future::result(Ok(
        HttpResponse::InternalServerError().into(),
      )));
    }
  };

  let content: data::models::BookBody = match serde_json::from_str(&body) {
    Ok(content) => content,
    Err(_) => return bad_format("".to_string()),
  };

  let book = BookUpdate {
    id: book.id,
    body: serde_json::to_value(content).unwrap(),
    title: book.title.clone(),
    cover: book.cover.clone(),
    release_date: book.release_date.clone(),
    lang: book.lang.clone(),
    keywords: book.keywords.clone(),
    slug: book.slug.clone(),
    summary: book.summary.clone(),
    author_id: book.author_id.clone(),
  };

  state
    .db
    .send(book)
    .from_err()
    .and_then(|res| match res {
      Ok(_id) => Ok(HttpResponse::Ok().into()),
      Err(_) => Ok(HttpResponse::InternalServerError().into()),
    })
    .responder()
}

pub fn list_book(req: &HttpRequest<AppState>) -> APIResponse {
  req
    .state()
    .db
    .send(BookList)
    .from_err()
    .and_then(|res| match res {
      Ok(books) => Ok(HttpResponse::Ok().json(books)),
      Err(_) => Ok(HttpResponse::InternalServerError().into()),
    })
    .responder()
}
