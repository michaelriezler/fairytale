import React, { useState } from 'react'
import { Auth } from 'Data/Http'

export default (props) => {

  let [form, setForm] = useState({
    username: '',
    password: ''
  })

  function handleChange ({ target }) {
    setForm({ ...form, [target.name]: target.value })
  }

  function handleAuth () {
    Auth.test(form).then(redirect => {
      if (redirect) props.history.push('/dashboard')
    })
  }

  return (
    <div>
      <input type="text" name='username' onChange={handleChange} value={form.username}/>
      <input type="password" name='password' onChange={handleChange} value={form.password}/>
      <button onClick={handleAuth}>Login</button>
    </div>
  )
}