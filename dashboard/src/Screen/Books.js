import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Book } from 'Data/Http'

import {
  DataTable,
  DataTableContent,
  DataTableHead,
  DataTableBody,
  DataTableHeadCell,
  DataTableRow,
  DataTableCell
} from '@rmwc/data-table';

let StyledTable = styled(DataTable)`
  width: 100%;

  table {
    width: 100%;
  }
`

let Row = ({ id, title, children }) => {
  return (
    <DataTableRow >
      <DataTableCell>{id}</DataTableCell>
      <DataTableCell>{title}</DataTableCell>
      <DataTableCell>{children}</DataTableCell>
    </DataTableRow>
  )
}

let Button = styled.button`
  margin-bottom: 24px;
`

export default ({ history }) => {
  let [books, set_books] = useState([])
  useEffect(() => {
    Book
      .get_all()
      .then(res => res.json())
      .then(data => set_books(data))
  }, [])

  return (
    <React.Fragment>
      <div>
        <Button onClick={() => history.push('/book/new')}>Add new</Button>
      </div>

      <StyledTable>
        <DataTableContent>
          <DataTableHead >
            <DataTableRow>
              <DataTableHeadCell>ID</DataTableHeadCell>
              <DataTableHeadCell>Title</DataTableHeadCell>
              <DataTableHeadCell></DataTableHeadCell>
            </DataTableRow>
          </DataTableHead>
          <DataTableBody>
            {books.map((book, i) => {

              return (
                <Row key={book.id} {...book}>
                  <button onClick={() => history.push(`/book/${book.id}`)}>Edit</button>
                </Row>
              )

            })}
          </DataTableBody>
        </DataTableContent>
      </StyledTable>
    </React.Fragment>
  )
}
