import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Book, Author } from 'Data/Http'

let Input = styled.input`
  width: 100%;
`

let Section = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 24px
`

export default (props) => {

  let [authors, set_authors] = useState([])

  useEffect(() => {
    Author
      .get_all()
      .then(res => res.json())
      .then(data => set_authors(data))
  }, [props.match.params.id])

  let [input, set_input] = useState({
    slug: '',
    title: '',
    cover: '',
    lang: 'en',
    author_id: '',
    body: [],
    release_date: '',
    summary: '',
    created_at: '',
    updated_at: '',
    keywords: ''
  })

  function handle_change ({ target }) {
    set_input({ ...input, [target.name]: target.value })
  }

  function handle_submit (e) {
    e.preventDefault()

    if (props.match.params.id === 'new') {
      Book
        .create(input)
        .then(res => res.json())
        .then(id => {
          props.history.push(`/book/${id}`)
        })
    } else {
      Book
        .update(input)
        .then(res => console.log('OK'))
        .catch(err => console.error(err))
    }

  }


  useEffect(() => {

    let { id } = props.match.params
    if (id === 'new') return

    Book
      .get(id)
      .then(res => res.json())
      .then(data => {
        let [book, authors] = data
        set_input({
          ...book,
          author_id: authors.author_id
        })
      })

  }, [props.match.params.id])

  return (
    <React.Fragment>
        
      <form onSubmit={handle_submit}>

        <button type='submit'>{
          props.match.params.id === 'new'
            ? 'Save'
            : 'Update'
        }</button>

        <Section>
          <label htmlFor="title">Title:</label>
          <Input type="text" name='title' value={input.title} onChange={handle_change} />
        </Section>

        <Section>
          <label htmlFor="slug">Slug:</label>
          <Input type="text" name='slug' value={input.slug} onChange={handle_change} />
        </Section>

        <Section>
          <label htmlFor="cover">Cover:</label>
          <Input type="text" name='cover' value={input.cover} onChange={handle_change} />
        </Section>        

        <Section>
          <label htmlFor="lang">Language:</label>
          <Input type="text" name='lang' value={input.lang} onChange={handle_change} />
        </Section>

        <Section>
          <label htmlFor="author_id">Author:</label>
          <select name="author_id" id="author_id" value={input.author_id} onChange={handle_change}>
            { authors.map(a => <option key={a.id} value={a.id}>{ a.name }</option>)}
          </select>
        </Section>

        <Section>
          <label htmlFor="release_date">Release Date:</label>
          <Input type="text" name='release_date' value={input.release_date} onChange={handle_change} />
        </Section>

        <Section>
          <label htmlFor="keywords">Keywords:</label>
          <Input type="text" name='keywords' value={input.keywords} onChange={handle_change} />
        </Section>

        <Section>
          <label htmlFor="summary">Summary:</label>
          <textarea type="text" name='summary' value={input.summary} onChange={handle_change}/>
        </Section>

        <BookBody
          value={input.body}
          name='body'
          onChange={handle_change}
        ></BookBody>

      </form>

    </React.Fragment>
  )
}


let BodySection = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 32px;
`

let BookBody = ({ value, name, onChange }) => {

  let [add_chapter, set_add_chapter] = useState({
    open: false,
    title: '',
    value: ''
  })

  function handle_new ({ target }) {
    set_add_chapter({ ...add_chapter, [target.name]: target.value })
  }

  function handle_change (index) {
    return function ({ target }) {
      let change = value[index]
      let new_value = { ...change, [target.name]: target.value }
      
      let _target = {
        name,
        value: [
          ...value.slice(0, index),
          new_value,
          ...value.slice(index + 1)
        ]
      }

      onChange({ target: _target })
    }
  }

  function open (e) {
    e.preventDefault()
    set_add_chapter({ ...add_chapter, open: true })
  }

  function add (e) {
    e.preventDefault()
    let { open, ...new_chapter } = add_chapter
    
    let target = {
      name,
      value: [
        ...value,
        new_chapter
      ]
    }

    onChange({ target })

    set_add_chapter({ open: false, title: '', value: '' })
  }

  return (
    <Section>
      { value.map((chapter, index) => (
          <BodySection key={chapter.title}>
            <label>Title</label>
            <input type="text" name='title' onChange={handle_change(index)} value={chapter.title} />
            <label>Value</label>
            <textarea type="text" name='value' onChange={handle_change(index)} defaultValue={chapter.value} />
          </BodySection>
      ))}

      { add_chapter.open &&
        <BodySection>
          <label>Title</label>
          <input type="text" name='title' onChange={handle_new} value={add_chapter.title} />
          <label>Value</label>
          <textarea type="text" name='value' onChange={handle_new} defaultValue={add_chapter.value}></textarea>
          <button onClick={add}>Add</button>
        </BodySection>
      }

      { !add_chapter.open &&
        <button onClick={open}>Add new</button>
      }
    </Section>
  )
}