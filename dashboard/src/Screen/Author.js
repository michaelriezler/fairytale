import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Author } from 'Data/Http'

import {
  DataTable,
  DataTableContent,
  DataTableHead,
  DataTableBody,
  DataTableHeadCell,
  DataTableRow,
  DataTableCell
} from '@rmwc/data-table';

let StyledTable = styled(DataTable)`
  width: 100%;

  table {
    width: 100%;
  }
`

let Row = ({ name, username, id, children, bio }) => {
  return (
    <DataTableRow >
      <DataTableCell>{id}</DataTableCell>
      <DataTableCell>{name}</DataTableCell>
      <DataTableCell>{username}</DataTableCell>
      <DataTableCell>{bio}</DataTableCell>
      <DataTableCell>{children}</DataTableCell>
    </DataTableRow>
  )
}

let Button = styled.button`
  margin-bottom: 24px;
`

let default_author = { username: '', name: '', bio: '' }

export default () => {

  let [new_author, set_new_author] = useState(default_author)
  let [add_new, set_add_new] = useState(false)
  let [reload, set_reload] = useState(true)

  function handle_change ({ target }) {
    set_new_author({ ...new_author, [target.name]: target.value })
  }

  function save (author) {
    Author
      .create(author)
      .then(res => res.json())
      .then(id => {
        console.log(id)
        set_new_author(default_author)
        set_add_new(false)
        set_reload(!reload)
      })
  }

  let [edit, set_edit] = useState({
    id: '',
    name: '',
    username: '',
    bio: '',
  })

  function handle_update_change ({ target }) {
    set_edit({ ...edit, [target.name]: target.value })
  }

  function update (author) {
    Author
      .update(author)
      .then(_res => {
        set_edit({
          id: '',
          name: '',
          username: '',
          bio: '',
        })
        set_add_new(false)
        set_reload(!reload)
      })
  }

  let [authors, set_authors] = useState([])
  useEffect(() => {
    Author
      .get_all()
      .then(res => res.json())
      .then(data => set_authors(data))
  }, [reload])

  return (
    <React.Fragment>
      <div>
        <Button onClick={() => set_add_new(true)}>Add new</Button>
      </div>

      <StyledTable>
        <DataTableContent>
          <DataTableHead >
            <DataTableRow>
              <DataTableHeadCell>ID</DataTableHeadCell>
              <DataTableHeadCell>Name</DataTableHeadCell>
              <DataTableHeadCell>Username</DataTableHeadCell>
              <DataTableHeadCell>Bio</DataTableHeadCell>
              <DataTableHeadCell></DataTableHeadCell>
            </DataTableRow>
          </DataTableHead>
          <DataTableBody>
            {
              add_new &&
              <Row 
                name={<input type='text' onChange={handle_change} value={new_author.name} name='name' />}
                username={<input type='text' onChange={handle_change} value={new_author.username} name='username' />}
                bio={<input type='text' onChange={handle_change} value={new_author.bio} name='bio' />}
              >
                <DataTableCell>
                  <button onClick={() => save(new_author)}>Save</button>
                </DataTableCell>
              </Row>
            }


            {authors.map((author, i) => {

              if (author.id === edit.id) {
                return (
                  <Row
                    key={edit.id}
                    id={edit.id}
                    name={<input type='text' onChange={handle_update_change} value={edit.name} name='name' />}
                    username={<input type='text' onChange={handle_update_change} value={edit.username} name='username' />}
                    bio={<input type='text' onChange={handle_update_change} value={edit.bio} name='bio' />}
                  >
                    <button onClick={() => update(edit)}>Save</button>
                  </Row>
                )
              }

              return (
                <Row key={author.id} {...author}>
                  <button onClick={() => set_edit(author)}>Edit</button>
                </Row>
              )

            })}
          </DataTableBody>
        </DataTableContent>
      </StyledTable>
    </React.Fragment>
  )
}
