import React from 'react'
import ReactDOM from 'react-dom'

import '@material/drawer/dist/mdc.drawer.css'
import '@material/list/dist/mdc.list.css'
import '@rmwc/data-table/data-table.css'
import '@material/checkbox/dist/mdc.checkbox.css'
import '@material/form-field/dist/mdc.form-field.css'
import '@material/select/dist/mdc.select.css'
import '@material/floating-label/dist/mdc.floating-label.css'
import '@material/notched-outline/dist/mdc.notched-outline.css'
import '@material/line-ripple/dist/mdc.line-ripple.css'
import '@material/switch/dist/mdc.switch.css'
import '@material/form-field/dist/mdc.form-field.css'
import './index.css'

import App from './App'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import Store from 'Data/Redux'

let Main = (
  <Provider store={Store}>
    <BrowserRouter>
      <App></App>
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(Main, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
