import React from 'react'
import { BrowserRouter, Switch, Link } from 'react-router-dom'
import styled from 'styled-components'

import {
  Drawer,
  DrawerHeader,
  DrawerContent,
  DrawerTitle,
  DrawerSubtitle
} from '@rmwc/drawer'

import {
  List,
  ListItem
} from '@rmwc/list'

let StyledDrawer = styled(Drawer)`
  height: 100vh;
  position: fixed;
  left: 0;
`

let Wrapper = styled.div`
  display: flex;
  min-height: 100%;
  margin-left: 260px;
`

let ContentWrapper = styled.div`
  flex-grow: 1;
  margin: 24px;
  padding: 16px;
  background: #fff;
  border-radius: 8px;
`

export default (props) => {
    return (
      <BrowserRouter basename="/dashboard">
        <Wrapper>
          <StyledDrawer>
            <DrawerHeader>
              <DrawerTitle>Fairytail</DrawerTitle>
              <DrawerSubtitle>development</DrawerSubtitle>
            </DrawerHeader>
            <DrawerContent>
              <List>
                <Link to='/authors'>
                  <ListItem>Authors</ListItem>
                </Link>
                <Link to='/books'>
                  <ListItem>Books</ListItem>
                </Link>
              </List>
            </DrawerContent>
          </StyledDrawer>

          <ContentWrapper>
            <Switch>
              { props.children }
            </Switch>
          </ContentWrapper>
        </Wrapper>
      </BrowserRouter>
  )
}