import { createStore, combineReducers } from 'redux'
import * as A from 'Action'

let UserState = {
  is_authenticated: false
}

function User (state = UserState, action) {
  switch (action.type) {
    case A.USER_AUTHENTICATE:
      return { ...state, is_authenticated: true }

    default:
      return state
  }
}

let reducer = combineReducers({ User })

let dev_tools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

export default createStore(reducer, dev_tools)
