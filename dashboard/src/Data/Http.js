import qs from 'query-string'
import Store from 'Data/Redux'

let Http = {
  create (base_url) {

    function get_headers () {
      let username = sessionStorage.getItem('username')
      let password = sessionStorage.getItem('password')

      return {
        'AUTHORIZATION': `Basic: ${username}:${password}`,
        'content-type': 'application/json'
      }
    }

    function get (url = '/', query = {}) {
      return fetch(`${base_url}${url}?${qs.stringify(query)}`, {
        headers: get_headers(),
      })
    }

    function post (url = '/', body = {}) {
      return fetch(`${base_url}${url}`, {
        method: 'POST',
        headers: get_headers(),
        body: JSON.stringify(body)
      })
    }

    return {
      get,
      post,
    }
  }
}

let api = Http.create(process.env.REACT_APP_API_URL)

export let Book = {
  get: id => api.get('/book/get', { id }),
  get_all: () => api.get('/book/list'),
  create: book => api.post('/book/create', book),
  update: book => api.post('/book/update', book),
}

export let Author = {
  get: id => api.get('/author/get', { id }),
  get_all: () => api.get('/author/list'),
  create: author => api.post('/author/create', author),
  update: author => api.post('/author/update', author)
}

export let Auth = {
  test: ({ username, password }) => {
    sessionStorage.setItem('username', username)
    sessionStorage.setItem('password', password)
    return api.get('/admin/auth/test').then(async res => {
      if (res.ok) {
       Store.dispatch({ type: 'USER_AUTHENTICATE' })
       return true
      }

      await res.text()
      return false
    })
  }
}