import React from 'react'
import { Redirect, withRouter, Route } from 'react-router-dom'
import { connect } from 'react-redux'

function PrivateRoute ({ children: Component, is_authenticated, ...rest }) {
  return (
      <Route
        {...rest}
        render={({ staticContext, ...props }) =>
          is_authenticated ? (
            React.cloneElement(Component, props)
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
}

export default withRouter(connect(state => {
  return { is_authenticated: state.User.is_authenticated }
})(PrivateRoute))
