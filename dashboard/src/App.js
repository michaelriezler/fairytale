import React, { useState, useEffect } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import PrivateRoute from 'Component/PrivateRoute'
import Login from 'Screen/Login'
import AppShell from 'AppShell'
import Author from 'Screen/Author'
import Books from 'Screen/Books'
import Book from 'Screen/Book'

import { Auth } from 'Data/Http'

export default withRouter((props) => {

  let [loading, setLoading] = useState(true)

  useEffect(() => {
    let username = sessionStorage.getItem('username')
    let password = sessionStorage.getItem('password')

    Auth.test({ username, password }).then(redirect => {
      redirect && props.history.push('/dashboard')
      setLoading(false)
    })
  }, [])


  if (loading) {
    return <div>loading</div>
  }

  return (
    <Switch>

      <PrivateRoute path='/dashboard'>
        <AppShell>
          <Route path='/authors' component={Author} />
          <Route path='/book/:id' component={Book} />
          <Route path='/books' component={Books} />
          <Route render={() => <div>Home</div>} />
        </AppShell>
      </PrivateRoute>

      <Route path='/login' component={Login} />

      <Redirect to='/dashboard' />
    </Switch>
  )
})
