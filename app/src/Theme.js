

export default {
  dark: {
    body_background: '#001018',
    text_color: '#ddd',
    headline_color: '#eee',
    sidebar_color: '#131313',
  },
  light: {
    body_background: '#fff',
    text_color: '#444',
    headline_color: '#222',
    sidebar_color: '#fafafa',
  },
}