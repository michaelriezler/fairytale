import React from 'react'
import styled, { createGlobalStyle } from 'styled-components'

let FixedBody = createGlobalStyle`
  body {
    ${p => p.open ? 'overflow: hidden;' : ''}
  }
`

let Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  position: fixed;
  bottom: 0;
  left: 0;
  background: var(--secondary-color);
  position: fixed;
  display: flex;
  flex-direction: column;
  transform: translateY(${p => p.open ? '0' : `100%`});
  transition: transform 500ms cubic-bezier(0.36, 0.68, 0.74, 0.96);
  box-shadow: 0 7px 10px 5px rgba(100,100, 100, 0.4);
  background: var(--body-background);
  padding: 10px;
`

let ContentWrapper = styled.div`
  width: 100%;
  flex-grow: 1;
  overflow-y: scroll;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
`

export default ({
  children,
  coins,
  open,
  onChange,
  title
}) => {
  return (
    <Wrapper open={open}>
      <FixedBody open={open}></FixedBody>
      <ContentWrapper>{ children }</ContentWrapper>
    </Wrapper>
  )
}