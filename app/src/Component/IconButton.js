import React from 'react'
import styled from 'styled-components'

let StyledIconButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  padding: 0;
`

export default ({ children, ...props }) => {
  return (
    <StyledIconButton { ...props }>
      { children }
    </StyledIconButton>
  )
}