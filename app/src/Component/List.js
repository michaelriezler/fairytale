import React from 'react'
import styled from 'styled-components'

export let List = styled.ol`
  padding-left: 30px;
`

export let StyledListItem = styled.li`
  margin-bottom: 20px;
`

export let Content = styled.div`
  display: flex;
  margin-left: 10px;
  color: #000;
`

export let ListItem = ({ children, ...props }) => (
  <StyledListItem>
    <Content {...props}>{ children }</Content>
  </StyledListItem>
)