import React, { useState } from 'react'


export default ({ value }) => {
  let [open, set_open] = useState(false)

  let text = open ? value : value.slice(0, 500)

  return (
    <div>
      <p>{ text }</p>
      { !open &&
        <button onClick={() => set_open(true)}>more</button>
      }
    </div>
  )
}