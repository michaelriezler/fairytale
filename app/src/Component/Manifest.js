import React from 'react'
import Icon_192x192 from 'Asset/android-chrome-192x192.png'
import Icon_512x512 from 'Asset/android-chrome-512x512.png'

export function make_manifest ({
  startUrl = window.location.href,
  name = '',
  display = 'standalone',
  themeColor = '#000000',
  backgroundColor = '#ffffff',
  description = '',
  shortName = ''
}) {

  let stringManifest = JSON.stringify({
    start_url: startUrl,
    name,
    display,
    theme_color: themeColor,
    background_color: backgroundColor,
    description,
    short_name: shortName,
    icons: [{
      "src": Icon_192x192,
      "sizes": "192x192",
      "type": "image/png"
    },{
      "src": Icon_512x512,
      "sizes": "512x512",
      "type": "image/png"
    },]
  })

  let blob = new Blob([stringManifest], { type: 'application/json' })
  let manifestURL = URL.createObjectURL(blob))
  return manifestURL
}

export default (props) => {
  return <link rel="manifest" href={make_manifest(props)} />
}