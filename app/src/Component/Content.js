import styled from 'styled-components'

export default styled.main`
  margin-top: 50px;
  padding: 0 10px;
  width: 100%;
`
