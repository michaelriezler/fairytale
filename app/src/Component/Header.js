import React from 'react'
import { useHideOnScroll } from 'Helper'
import styled from 'styled-components'

export let Header = styled.header`
  background: #000;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  display: flex;
  height: 50px;
  align-items: center;
  padding: 0 10px;
`


let StyledBookHeader = styled(Header)`
  background: var(--sidebar-color);
  transform: translateY(${p => p.hide ? '-100%' : 0 });
  transition: transform 500ms;
`

export let BookHeader = ({ children }) => {
  let hide = useHideOnScroll()

  return (
    <StyledBookHeader hide={hide}>
      { children }
    </StyledBookHeader>
  )
}