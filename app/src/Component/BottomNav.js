import React from 'react'
import { useHideOnScroll } from 'Helper'
import styled from 'styled-components'


let StyledBottomNav = styled.nav`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 50px;
  background: #fff;

  display: flex;
  align-items: center;
  justify-content: space-around;

  @media screen and (min-width: 600px) {
    justify-content: flex-start;
  }

  transform: translateY(${p => p.hide ? '100%' : 0 });
  transition: transform 500ms;
`

export default ({
  hideOnScroll = true,
  children,
  ...props
}) => {
  
  let hide = useHideOnScroll(hideOnScroll)

  return (
    <StyledBottomNav hide={hide} {...props}>
      { children }
    </StyledBottomNav>
  )
}