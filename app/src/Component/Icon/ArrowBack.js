import React from 'react'
import styled from 'styled-components'
import { is_ios } from 'Helper'

let Svg = styled.svg`
  fill: #fff;
`

let AndoridIcon = () => {
  return (
    <Svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
      <path d="M0 0h24v24H0z" fill="none"/>
      <path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/>
    </Svg>
  )
}


let IosIcon = () => {
  return (
    <Svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
      <path d="M11.67 3.87L9.9 2.1 0 12l9.9 9.9 1.77-1.77L3.54 12z"/>
      <path fill="none" d="M0 0h24v24H0z"/>
    </Svg>
  )
}


export default () => {
  return is_ios() ? <IosIcon /> : <AndoridIcon />
}