import styled from 'styled-components'

export let Title = styled.h1.attrs({ className: 'typl8-gamma' })`
  text-align: center;
  hyphens: none;
`