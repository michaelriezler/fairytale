import React from 'react'
import ReactDOM from 'react-dom'
import 'Styles/Typeplate.css'
import 'react-toastify/dist/ReactToastify.css'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter } from 'react-router-dom'
import { HistoryLength } from 'Helper/Context'

let history_length = window.history.length

let Main = (
  <BrowserRouter>
    <HistoryLength.Provider value={history_length}>
      <App />
    </HistoryLength.Provider>
  </BrowserRouter>
)

ReactDOM.render(Main, document.getElementById('root'))

serviceWorker.register()
