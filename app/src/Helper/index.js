import { useContext, useState, useEffect } from 'react'
import useReactRouter from 'use-react-router'
import { HistoryLength } from 'Helper/Context'
import _ from 'lodash'

export function is_ios () {

  let devices = [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ]

  if (navigator.platform) {
    return devices.includes(navigator.platform)
  } else {
    return false
  }
}

export function useGoBack (fallback = '/') {
  let { history } = useReactRouter()
  let initial_length = useContext(HistoryLength)
  return function go_back () {
    if (initial_length === history.length) {
      history.replace(fallback)
    } else {
      history.goBack()
    }
  }
}


export function useOnline () {
  let [online, setOnline] = useState(true)

  function updateOnlineStatus(event) {
    setOnline(navigator.onLine)
  }

  useEffect(() => {
    window.addEventListener('online',  updateOnlineStatus);
    window.addEventListener('offline', updateOnlineStatus);
  }, [])

  return online
}


export function set_css_property (node, prop, value) {
  node.style.setProperty(`--${prop}`, value)
}

export function useA2HS () {
  let [prompt, setPrompt] = useState({})

  useEffect(() => {
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault()
      setPrompt(e)
    })
  }, [])

  return prompt
}


export function useHideOnScroll (hideOnScroll = true) {
  let [hide, set_hide] = useState(false)

  useEffect(() => {

    let prev_scroll_position = window.scrollY

    let handle_scroll = _.debounce(() => {
      let scroll = window.scrollY
      let scroll_down = prev_scroll_position < scroll
      if (scroll_down) {
        if (document.documentElement.scrollHeight - window.innerHeight < scroll + 300) {
          set_hide(false)
        } else {
          set_hide(true)
        }
      } else {
        set_hide(false)
      }

      prev_scroll_position = scroll
    }, 100)

    hideOnScroll && window.addEventListener('scroll', handle_scroll)
    return () => {
      hideOnScroll && window.removeEventListener('scroll', handle_scroll)
    }
  }, [])

  return hide
}