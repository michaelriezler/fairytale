import { StorageArea } from 'std:kv-storage'

let default_storage = new StorageArea('ft-default-storage')

export function storage_first ({
  key,
  storage = default_storage,
  loader
}) {
  let loader_data = false
  let storage_promise = storage.get(key)

  let req = loader()
    .then(data => {
      loader_data = true
      storage.set(key, data)
      return { data, storage: false }
    })

  return storage_promise
    .then(data => {
      if (data !== undefined && loader_data === false) {
        return { data, storage: true }
      } else {
        return req
      }
    })
}