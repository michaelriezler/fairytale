import { createContext } from 'react'

export let HistoryLength = createContext(0)