import qs from 'query-string'
import { StorageArea } from 'std:kv-storage'

let Http = {
  create (base_url) {

    function get_headers () {
      return {
        'content-type': 'application/json'
      }
    }

    function get (url = '/', query = {}) {
      return fetch(`${base_url}${url}?${qs.stringify(query)}`, {
        headers: get_headers(),
      })
    }

    function post (url = '/', body = {}) {
      return fetch(`${base_url}${url}`, {
        method: 'POST',
        headers: get_headers(),
        body: JSON.stringify(body)
      })
    }

    return {
      get,
      post,
    }
  }
}

let api = Http.create('http://localhost:3001')

export let Book = {
  get_all: () => api.get('/book', {
    select: `title,slug,lang,release_date,summary,author(name, username)`,
    order: 'title.asc'
  }),

  get_info: (slug) => api.get('/book', {
    select: 'id,title,summary,slug,release_date,lang,author(name),keywords',
    slug: `eq.${slug}`
  }),

  get_by_slug: slug => api.get('/book',  {
    slug: `eq.${slug}`,
    select: 'id,title,release_date,body,summary,author(name),keywords',
  })
}

export let Author = {
  get_all: () => api.get('/author', {
    order: 'name.asc',
    select: 'name,username,bio'
  }),
  
  get_by_username: username => api.get('/author',  {
    username: `eq.${username}`,
    select: '*,book(title,slug)'
  }),

}


export let ReadingList = new StorageArea('ft-reading-list')
export let Books = new StorageArea('ft-books')
export let BookInfo = new StorageArea('ft-book-info')