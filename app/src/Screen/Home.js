import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import "react-tabs/style/react-tabs.css"
import { Header } from 'Component/Header'
import { Book, Author, Books } from 'Data'
import { List, ListItem } from 'Component/List'
import { Link } from 'react-router-dom'
import Content from 'Component/Content'
import { useOnline } from 'Helper'
import { Helmet } from 'react-helmet'
import ShareIcon from 'Component/Icon/Share'
import AddToHomeScreenIcon from 'Component/Icon/AddToHomeScreen'
import IconButton from 'Component/IconButton'
import { make_manifest } from 'Component/Manifest'
import { useA2HS } from 'Helper'

let AddToHomeScreen = styled(IconButton)`
  svg {
    fill: #bfbfbf;
  }
`

let Share = styled(IconButton)`
  svg {
    fill: #bfbfbf;
  }
`

let StyledListItem = styled(ListItem)`
  flex-direction: column;
  max-width: 600px;
  margin-bottom: 40px;
  
  p {
    text-align: left;
    margin: 0;
  }
`

let StyledLink = styled(Link)`
  text-decoration: none;
`

let StyledTab = styled(Tab)`
  border: none;
  width: 150px;
  text-align: center;
  cursor: pointer;
  flex-shrink: 0;

  span {
    display: inline-block;
    padding: 10px;
    padding-bottom: 0;
    border-bottom: 1px solid rgba(0,0,0,0);
    color: #595959;
  }

  &.react-tabs__tab--selected span {
    border-bottom: 1px solid;
    color: #000;
  }
`

let StyledTabList = styled(TabList)`
  display: flex;
  list-style-type: none;
  padding: 0;
  margin: 0;
  margin-bottom: 30px;
  flex-wrap: nowrap;
  overflow-y: scroll;

  @media screen and (min-width: 550px) {
    justify-content: center;
  }
`

let HeaderTitle = styled.h5`
  color: #fff;
  margin: 0 auto;
`

let BookList = styled(List)`
  display: flex;
  flex-wrap: wrap;
`

let BookListItem = styled(ListItem)`
  max-width: 600px;
  flex-direction: column;
  margin-right: 40px;
`

let BookAuthor = styled.small`
  margin-bottom: 13px;
  color: #000;
  text-decoration: underline;
`

let ReadButton = styled(Link)`
  display: flex;
  justify-content: flex-end;
  text-transform: uppercase;
  letter-spacing: 2px;
  font-size: 14px;
  color: inherit;
`

let Head = React.memo(() => {
  return (
    <Helmet>
      <link rel="manifest" href={make_manifest({
        startUrl: '.',
        description: 'Free Online Books',
        name: `Fairytail`,
        shortName: 'Fairytail'
      })} />
    </Helmet>
  )
})

export default ({ history }) => {
  let [books, set_books] = useState([])
  let [authors, set_authors] = useState([])
  let [offline_books, set_offline_books] = useState([])

  let is_online = useOnline()
  let a2hs = useA2HS()

  useEffect(() => {
    Book
      .get_all()
      .then(res => res.json())
      .then(data => set_books(data))
    
    Author
      .get_all()
      .then(res => res.json())
      .then(data => set_authors(data))

    ;(async () => {
      let offline_books = []

      for await (const [value] of Books.values()) {
        offline_books.push(value)
      }

      offline_books.sort(function(a, b){
          if(a.title < b.title) { return -1 }
          if(a.title > b.title) { return 1 }
          return 0
      })

      set_offline_books(offline_books)
    })()
  }, [])

  return (
    <React.Fragment>
      <Head></Head>
      <Header>
        {
          a2hs.prompt &&
          <AddToHomeScreen onClick={() => a2hs.prompt()}>
            <AddToHomeScreenIcon></AddToHomeScreenIcon>
          </AddToHomeScreen>
        }
        <HeaderTitle className='typl8-zeta'>Fairytail</HeaderTitle>
        <Share onClick={() => {
          navigator.share({
              title: 'fairytail.app',
              text: `Read free ebooks at https://fairytail.app`,
              url: window.location.href
          })
        }}>
          <ShareIcon></ShareIcon>
        </Share>
      </Header>
      <Content>
        <Tabs>
          <StyledTabList defaultindex={is_online ? 0 : 2}>
            <StyledTab><span>Books</span></StyledTab>
            <StyledTab><span>Authors</span></StyledTab>
            <StyledTab><span>Downloads</span></StyledTab>
          </StyledTabList>

          <TabPanel>
            <BookList>{
              books.map(book => {
                return (
                  <BookListItem key={book.slug}>
                    <StyledLink to={`/book/info/${book.slug}`}>
                      <h5 style={{ margin: 0 }}>{ book.title }</h5>
                    </StyledLink>
                    <StyledLink to={`/author/${book.author[0].username}`}>
                      <BookAuthor>{ book.author[0].name }</BookAuthor>
                    </StyledLink>
                    <p>{ book.summary.slice(0, 160) }</p>
                    <ReadButton to={`/book/${book.slug}`}>
                      read
                    </ReadButton>
                  </BookListItem>
                )
              }) 
            }</BookList>
          </TabPanel>

          <TabPanel>
            <List>{
              authors.map(author =>
                <StyledLink to={`/author/${author.username}`} key={author.username}>
                  <StyledListItem>
                    <h5>{ author.name }</h5>
                    <p>{ author.bio.slice(0, 160) }</p>
                  </StyledListItem>
                </StyledLink>
              ) 
            }</List>
          </TabPanel>

          <TabPanel>
            <BookList>{
              offline_books.map(book => {
                return (
                  <BookListItem key={`of-${book.slug}`}>
                    <StyledLink to={`/book/info/${book.slug}`}>
                      <h5 style={{ margin: 0 }}>{ book.title }</h5>
                    </StyledLink>
                    <StyledLink to={`/author/${book.author[0].username}`}>
                      <BookAuthor>{ book.author[0].name }</BookAuthor>
                    </StyledLink>
                    <p>{ book.summary.slice(0, 160) }</p>
                    <ReadButton to={`/book/${book.slug}`}>
                      read
                    </ReadButton>
                  </BookListItem>
                )
              }) 
            }</BookList>
          </TabPanel>
        </Tabs>
      </Content>
    </React.Fragment>
  )
}