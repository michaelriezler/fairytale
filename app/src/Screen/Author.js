import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Author } from 'Data'
import { List, ListItem } from 'Component/List'
import { Link } from 'react-router-dom'
import { Header } from 'Component/Header'
import Content from 'Component/Content'
import ArrowBack from 'Component/Icon/ArrowBack'
import { Title } from 'Component/Typography'
import IconButton from 'Component/IconButton'
import { Helmet } from 'react-helmet'

let Wrapper = styled(Content)`
  max-width: 600px;
  margin: 0 auto;
`

let StyledListItem = styled(ListItem)`
  text-decoration: underline;
`

export default ({ match, history }) => {
  let [author, set_author] = useState({
    name: '',
    username: '',
    bio: '',
    book: []
  })

  useEffect(() => {
    Author
      .get_by_username(match.params.username)
      .then(res => res.json())
      .then(data => {
        set_author(data[0])
      })
  }, [match.params.username])

  return (
    <React.Fragment>
      <Helmet>
        <title>{ author.name }</title>
        <meta name='description' content={`${author.bio.slice(0, 157).trim()}...`} />
      </Helmet>

      <Header>
        <IconButton onClick={() => history.goBack() }>
          <ArrowBack />
        </IconButton>
      </Header>

      <Wrapper style={{ marginTop: 80 }}>
        <Title>{ author.name }</Title>
        <p>{ author.bio }</p>
        <List>
          { author.book.map(b =>
            <Link to={`/book/info/${b.slug}`}>
              <StyledListItem>{ b.title }</StyledListItem>
            </Link>
          )}
        </List>
      </Wrapper>
    </React.Fragment>
  )
}