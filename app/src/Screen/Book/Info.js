import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Book } from 'Data'
import { Header } from 'Component/Header'
import Content from 'Component/Content'
import Summary from 'Component/Summary'
import { Title } from 'Component/Typography'
import ArrowBack from 'Component/Icon/ArrowBack'
import IconButton from 'Component/IconButton'
import { useGoBack } from 'Helper'
import { Helmet } from 'react-helmet'
import { BookInfo } from 'Data'
import { storage_first } from 'Helper/Http'

let ReadBtn = styled.button`
  background: var(--color-higlight);
  color: #fff;
  width: calc(100% - 20px);
  border: none;
  bottom: 10px;
  left: 5px;
  height: 40px;
  border-radius: 8px;
  font-size: 18px;
  margin-bottom: 30px;
`

let Author = styled.span`
  text-align: center;
  margin-bottom: 30px;
  display: block;
`

let Wrapper = styled(Content)`
  padding: 20px;
  padding-top: 30px;
  padding-bottom: 60px;
  max-width: 600px;
  margin: 0 auto;
  margin-top: 50px;
`

export default ({ match, history }) => {

  let [book, set_book] = useState({
    id: '',
    title: '',
    summary: '',
    slug: '',
    release_date: '',
    lang: '',
    author: [{ name: '' }]
  })

  useEffect(() => {
    storage_first({
      key: match.params.slug,
      storage: BookInfo,
      loader: () => Book.get_info(match.params.slug).then(res => res.json())
    })
      .then(result => {
        let [info] = result.data
        set_book(info)
      })
  }, [match.params.slug])

  let go_back = useGoBack()

  return (
    <React.Fragment>
      <Helmet>
        <meta name='description' content={`${book.summary.slice(0, 157).trim()}...`}/>
        <meta name='keywords' content={book.keywords}/>
        <title>{ book.title } by { book.author[0].name }</title>
      </Helmet>

      <Header>
        <IconButton onClick={() => go_back()}>
          <ArrowBack />
        </IconButton>
      </Header>

      <Wrapper>
        <Title>{ book.title }</Title>
        { book.author.map(a => <Author>{ a.name }</Author>) }
        
        <Link to={`/book/${book.slug}`}>
          <ReadBtn>read now</ReadBtn>
        </Link>

        <Summary value={book.summary} />
      </Wrapper>
    </React.Fragment>
  )
}