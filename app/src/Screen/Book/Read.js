import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Book as B, Books } from 'Data'
import Markdown from 'react-markdown/with-html'
import BottomNav from 'Component/BottomNav'
import { BrowserRouter, Link, Switch, Route, withRouter, Redirect } from 'react-router-dom'
import BottomDrawer from 'Component/BottomDrawer'
import IconButton from 'Component/IconButton'
import CloseIcon from 'Component/Icon/Close'
import TOCIcon from 'Component/Icon/FormatListBullet'
import AddToHomeScreenIcon from 'Component/Icon/AddToHomeScreen'
import NightModeIcon from 'Component/Icon/NightMode'
import ShareIcon from 'Component/Icon/Share'
import { storage  } from 'std:kv-storage'
import _ from 'lodash'
import ArrowRight from 'Component/Icon/KeyboardArrowRight'
import { useMedia } from 'react-use-media'
import { Helmet } from 'react-helmet'
import { storage_first } from 'Helper/Http'
import { set_css_property, useA2HS } from 'Helper'
import { toast } from 'react-toastify'
import { make_manifest } from 'Component/Manifest'
import Theme from 'Theme'
import { BookHeader } from 'Component/Header'
import ArrowBack from 'Component/Icon/ArrowBack'

let HeaderTitle = styled.h6`
  text-overflow: ellipsis;
  margin: 0;
  white-space: nowrap;
  overflow: hidden;
`

let HomeButton = styled(IconButton)`
  margin-right: 20px;

  svg {
    fill: var(--text-color);
  }
`

let ROOT = document.documentElement

let NavButton = styled(IconButton)`
  margin: 0 25px;
`

let StyledTOC = styled.ol`
  a {
    color: inherit;
    text-decoration: none;
  }

  li {
    margin-bottom: 10px;
  }
`

let TOC = ({ items, onClick }) => {
  return (
    <StyledTOC>
      { items.map(item =>
          <li key={item.slug}>
            <Link
              to={`/${item.slug}`}
              onClick={onClick}
            >
              { item.title }
          </Link>
        </li>
      )}
    </StyledTOC>
  )
}

let TOCWrapper = styled.div`
  width: 250px;
  background: var(--sidebar-color);
  padding: 20px 15px;
  flex-shrink: 0;
  position: fixed;
  height: 100%;
`

let DrawerHeader = styled.div`
  display: flex;
  justify-content: flex-end;

  svg {
    fill: var(--text-color);
  }
`

let BookContent = styled.main`
  padding-left: 15px;
  padding-right: 40px;
  padding-top: 25px;
  text-align: justify;
  max-width: 600px;

  @media screen and (min-width: 600px) {
    margin-left: 25px;
  }

  @media screen and (min-width: 700px) {
    margin-left: 50px;
  }

  @media screen and (min-width: 800px) {
    margin-left: 250px;
  }
  
  @media screen and (min-width: 900px) {
    margin-left: 150px;
  }

  @media screen and (min-width: 1024px) {
    margin-left: calc(250px + 150px);
  }

  a {
    text-decoration: underline;
  }
`

let ChapterTitle = styled.h2.attrs({ className: 'typl8-gamma' })`
  text-align: center;
`

let BookTitle = styled.h1`
  text-align: center;
  hyphens: none;
`

let ReadNext = styled(Link)`
  display: inline-flex;
  align-items: center;
  color: inherit;
  width: 100%;
  justify-content: flex-end;
`

let Wrapper = styled.div`
  display: flex;
  padding-top: 50px;
  background: var(--body-background);
  color: var(--text-color);
  min-height: 100%;

  .typl8-tera, .typl8-giga, .typl8-mega, h1, .typl8-alpha, h2, .typl8-beta, h3, .typl8-gamma, h4, .typl8-delta, h5, .typl8-epsilon, .typl8-zeta, h6 {
    color: var(--headline-color);
  }
`

let StyledBottomNav = styled(BottomNav)`
  background: var(--sidebar-color);

  svg {
    fill: var(--text-color);
  }
`

function flatten(text, child) {
  return typeof child === 'string'
    ? text + child
    : React.Children.toArray(child.props.children).reduce(flatten, text)
}

function HeadingRenderer(props) {
  var children = React.Children.toArray(props.children)
  var text = children.reduce(flatten, '')
  var slug = text.toLowerCase().replace(/\W/g, '-')
  return React.createElement('h' + props.level, {id: slug}, props.children)
}

function set_color_scheme () {
  storage.get('color-scheme').then(scheme => {
    let theme = Theme[scheme] || Theme.light
    set_css_property(ROOT, 'body-background', theme.body_background)
    set_css_property(ROOT, 'text-color', theme.text_color)
    set_css_property(ROOT, 'headline-color', theme.headline_color)
    set_css_property(ROOT, 'sidebar-color', theme.sidebar_color)
  })
}


async function toggle_color_scheme () {
  let scheme = await storage.get('color-scheme')
  let theme = scheme === 'dark' ? 'ligth' : 'dark'
  await storage.set('color-scheme', theme)
  set_color_scheme()
}


function areEqual(prevProps, nextProps) {
  return (
    prevProps.book.id === nextProps.book.id ||
    prevProps.slug === nextProps.slug
  )
}

let Head = React.memo(({ book, slug }) => {
  return (
    <Helmet key={window.location.href}>
      <meta name='description' content={`${book.summary.slice(0, 157).trim()}...`}/>
      <meta name='keywords' content={book.keywords}/>

      <link rel="manifest" href={make_manifest({
        startUrl: `${window.location.origin}/book/${slug}`,
        description: book.summary.slice(0, 157).trim(),
        name: `${book.title} - fairytail`,
        shortName: book.title
      })} />

      <title>{ book.title } by { book.author[0].name }</title>
    </Helmet>
  )
}, areEqual)

export default ({ match, history }) => {

  let [drawer, set_drawer] = useState(false)

  let [loading, set_loading] = useState(true)

  let [book, set_book] = useState({
    id: '',
    body: []
  })

  let [toc, set_toc] = useState({
    chapters: [],
    index: {}
  })

  let display_toc = useMedia({ minWidth: 1024 })

  useEffect(() => {

    storage_first({
      key: match.params.slug,
      storage: Books,
      loader: () => B.get_by_slug(match.params.slug).then(res => res.json())
    })
      .then(async result => {
        let [book] = result.data
        set_book(book)

        set_toc({
          chapters: build_toc(book.body),
          index: build_index(book.body)
        })

        let progress = await storage.get(`progress:${match.params.slug}`)

        if (progress) {
          let slug = to_slug(book.body[progress].title)
          history.replace(`/book/${match.params.slug}/${slug}`)
        }

        set_loading(false)

        if (!result.storage) {
          setTimeout(() => {
            toast(`${book.title} is now available offline`, { className: 'ft-toast'})
          }, 100)
        }
      })
  }, [match.params.slug])

  useEffect(set_color_scheme, [])

  let a2hs = useA2HS()

  if (loading) return null

  return (
    <BrowserRouter basename={`/book/${match.params.slug}`}>
      <Wrapper>

        <Head
          book={book}
          slug={match.params.slug}
        ></Head>

        <BookHeader>
          <HomeButton onClick={() => history.replace('/')}>
            <ArrowBack />
          </HomeButton>
          <HeaderTitle>{book.title}</HeaderTitle>
        </BookHeader>

        { display_toc &&
          <TOCWrapper>
            <Link to={`/book/${to_slug(book.title)}/cover`}>
              <h4 style={{ textAlign: 'center', hyphens: 'none' }} className='typl8-epsilon'>{book.title}</h4>
            </Link>
            <h6 style={{ textAlign: 'center', hyphens: 'none' }}>Table of Content</h6>
            <TOC items={toc.chapters} onClick={() => set_drawer(false)}></TOC>
          </TOCWrapper>
        }

        <Book
          cover={() => (
            <BookContent>
              <BookTitle className='typl8-alpha'>{ book.title }</BookTitle>
              <TOC items={toc.chapters} onClick={() => set_drawer(false)}></TOC>
            </BookContent>
          )}
          index={toc.index}
          chapters={book.body}
          slug={match.params.slug}
        ></Book>

        <StyledBottomNav>
          { !display_toc &&
            <NavButton aria-label='Open Table of Content' onClick={() => set_drawer(true)}>
              <TOCIcon></TOCIcon>
            </NavButton>
          }

          {
            a2hs.prompt &&
            <NavButton
              onClick={() => a2hs.prompt()}
              aria-label='Add to Homescreen'>
              <AddToHomeScreenIcon></AddToHomeScreenIcon>
            </NavButton>
          }

          <NavButton
            aria-label='Share Book'
            onClick={() => {
              navigator.share({
                  title: book.title,
                  text: `Read ${book.title} from ${book.author} on https://fairytail.app`,
                  url: window.location.href
              })
            }}
          >
            <ShareIcon></ShareIcon>
          </NavButton>

          <NavButton
            aria-label='Switch Color Scheme'
            onClick={toggle_color_scheme}
          >
            <NightModeIcon></NightModeIcon>
          </NavButton>
        </StyledBottomNav>

        { !display_toc && 
          <BottomDrawer open={drawer}>
            <DrawerHeader>
              <IconButton aria-label='Close Bottom Drawer' onClick={() => set_drawer(false)}>
                <CloseIcon></CloseIcon>
              </IconButton>
            </DrawerHeader>
            <Link to={`/book/${to_slug(book.title)}/cover`}>
              <h4 style={{ textAlign: 'center', hyphens: 'none' }}>{book.title}</h4>
            </Link>
            <h6 style={{ textAlign: 'center', hyphens: 'none' }}>Table of Content</h6>
            <TOC items={toc.chapters} onClick={() => set_drawer(false)}></TOC>
          </BottomDrawer>
        }
      </Wrapper>
    </BrowserRouter>
  )
}

function build_index (chapters) {
  let index = {}
  chapters.forEach((chapter, i) => {
    index[to_slug(chapter.title)] = i
  })

  return index
}

function build_toc (body) {
  return body.map(chapter => {
    return {
      title: chapter.title,
      slug: to_slug(chapter.title)
    }
  })
}

function to_slug (str) {
  let slug = str
    .split(' ')
    .map(s => s.trim().toLowerCase())
    .join('-')

  return encodeURI(slug)
}

let Book = withRouter(({ cover, chapters, index, slug }) => {

  return (
    <React.Fragment>
      <Switch>
        <Route path='/cover' component={cover} />
        <Route render={({ history }) => <Chapter slug={slug} history={history} chapters={chapters} index={index} />}/>
      </Switch>

      <div style={{
        height: 80,
        flexShrink: 0
      }}/>
    </React.Fragment>
  )
})

let Chapter = ({ history, chapters, index, slug }) => {

  useEffect(() => {

    let handle_scroll = _.debounce(() => {
      storage.set(window.location.pathname, window.scrollY)
    }, 500)

    storage.get(window.location.pathname).then(scroll_state => {
      window.addEventListener('scroll', handle_scroll)
      setTimeout(() => {
        window.scroll({ top: scroll_state })
      }, 0)
    })

  }, [window.location.pathname])

  let chapter = history.location.pathname.slice(1)
  let idx = index[chapter]

  let item = chapters[idx]

  useEffect(() => { 
    storage.set(`progress:${slug}`, idx)
  }, [window.location.pathname])

  if (!item) {
    return <Redirect to='/cover' />
  }

  let next = chapters[idx + 1]

  return (
    <BookContent>
      <Helmet>
        <title>{ item.title } | fairytail.app</title>
      </Helmet>
      <ChapterTitle>{ item.title }</ChapterTitle>
      <Markdown
        source={item.value}
        renderers={{ heading: HeadingRenderer }} escapeHtml={false}
      />


        <footer style={{ marginBottom: 80, marginTop: 50 }}>
        { next &&
          <ReadNext to={`/${to_slug(next.title)}`}>{next.title} <ArrowRight /></ReadNext>
        }
        </footer>

    </BookContent>
  )
}
