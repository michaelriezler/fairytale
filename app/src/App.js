import React, { useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import Loadable from 'react-loadable'
import { ToastContainer, toast } from 'react-toastify'
import { useOnline } from 'Helper'
import { Helmet } from 'react-helmet'
import apple_touch_icon from 'Asset/apple-touch-icon.png'

let Home = Loadable({
  loader: () => import('Screen/Home'),
  loading: () => <div>...Home</div>
})

let BookRead = Loadable({
  loader: () => import('Screen/Book/Read'),
  loading: () => <div>...Screen/Book/Read</div>
})

let BookInfo = Loadable({
  loader: () => import('Screen/Book/Info'),
  loading: () => <div>...Screen/Book/Info</div>
})

let Author = Loadable({
  loader: () => import('Screen/Author'),
  loading: () => <div>...Screen/Author</div>
})

export default () => {
  
  useEffect(() => {
    Home.preload()
    BookRead.preload()
    BookInfo.preload()
    Author.preload()
  }, [])

  let is_online = useOnline()

  useEffect(() => {
    !is_online && toast('You are currently offline')
  }, [is_online])
  
  return (
    <React.Fragment>

      <Helmet>
        <link rel="shortcut icon" href={`${apple_touch_icon}?v=1`} />
        <link rel="apple-touch-icon" href={`${apple_touch_icon}?v=1`} />
        <meta name="apple-mobile-web-app-status-bar-style" content="#000000" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-title" content="Fairytail" />
        <title>Fairytail</title>
      </Helmet>

      <Switch>
        <Route path='/book/info/:slug' component={BookInfo} />
        <Route path='/book/:slug' component={BookRead} />
        <Route path='/author/:username' component={Author} />
        <Route path='/' component={Home} />
      </Switch>
      <ToastContainer />
    </React.Fragment>
  )
}