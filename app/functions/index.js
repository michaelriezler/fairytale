let functions = require('firebase-functions');
let cors = require('cors')
let app = require('express')()

app.use(cors())
app.use('/manifest.json', (request, response) => {

  let {
    start_url = '.'
  } = request.query

  console.log('START_URL', start_url)

  response.json({
    "short_name": "React App",
    "name": "Create React App Sample",
    "icons": [
      {
        "src": "favicon.ico",
        "sizes": "64x64 32x32 24x24 16x16",
        "type": "image/x-icon"
      }
    ],
    start_url,
    "display": "standalone",
    "theme_color": "#000000",
    "background_color": "#ffffff"
  })
  
})

exports.manifest_generator = functions.https.onRequest(app)
